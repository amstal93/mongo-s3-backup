FROM mongo:4.4.1

RUN apt-get update && apt-get install -y python-pip cron
RUN rm -rf /var/lib/apt/lists/*
RUN pip install awscli
RUN mkdir /script

COPY start.sh /script
COPY backup.sh /script
COPY restore.sh /script
COPY get_bucket_region.py /script

ENTRYPOINT ["/script/start.sh"]
